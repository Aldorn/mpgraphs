//
// Created by max on 28.10.22.
//

#include <range/v3/all.hpp>

#include "autotest.hpp"
#include "mpgraphs/mapgraph.hpp"

template<mpgraphs::EdgeDirection Dir>
void testGraphRandomly() {
    const size_t NUM_SAMPLES = 20;
    using namespace mpgraphs;
    for (size_t i = 0; i < NUM_SAMPLES; ++i) {
        std::mt19937 rng((4711 * i + 1234) % 1337);
        MapGraph<size_t, Empty, Dir> graph;
        SimpleGraph<size_t, Dir> reference;
        size_t numVertices = randInt(rng, 50);
        size_t numEdges = randInt(rng, numVertices * numVertices / 2);
        {
            SCOPED_TRACE(fmt::format("graph with {} vertices and {} edges", numVertices, numEdges));
            testAdding(graph, reference, numVertices, numEdges, rng);
            testInteract(graph, reference, rng);
            testRemoving(graph, reference, rng);
        }
    }
}

TEST(MapGraph, testAutomaticUndirected) {
    testGraphRandomly<EdgeDirection::Undirected>();
}

TEST(MapGraph, testAutomaticDirected) {
    testGraphRandomly<EdgeDirection::Directed>();
}

TEST(MapGraph, testAutomaticBidirected) {
    testGraphRandomly<EdgeDirection::Bidirectional>();
}


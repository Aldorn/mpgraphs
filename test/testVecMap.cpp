//
// Created by max on 07.11.22.
//
#include <gtest/gtest.h>
#include <fmt/format.h>
#include "mpgraphs/vecmap.hpp"

struct DeStruct {
    static int& instances() {
        static int instances = 0;
        return instances;
    }
    static int& total() {
        static int total = 0;
        return total;
    }

    int value;
    int index;

    DeStruct() : DeStruct(-1) { }

    DeStruct(int value) : value(value), index(++total()) {
        ++instances();
    }

    DeStruct(const DeStruct& other) : DeStruct(other.value) { }
    DeStruct& operator=(const DeStruct& other) {
        value = other.value;
        return *this;
    }

    virtual ~DeStruct() {
        --instances();
    }

    friend auto operator <=>(const DeStruct& a, const DeStruct& b);
};

TEST(DeStruct, testDeStruct) {
    DeStruct::total() = 0;
    DeStruct::instances() = 0;
    DeStruct* reserved = (DeStruct*)malloc(sizeof(DeStruct));
    {
        DeStruct blub;
        EXPECT_EQ(blub.index, 1);
        EXPECT_EQ(DeStruct::instances(), 1);
        EXPECT_EQ(DeStruct::total(), 1);
        DeStruct bla(blub);
        EXPECT_EQ(bla.index, 2);
        EXPECT_EQ(DeStruct::instances(), 2);
        EXPECT_EQ(DeStruct::total(), 2);
        std::construct_at(reserved, 3);
        EXPECT_EQ(reserved->index, 3);
        EXPECT_EQ(DeStruct::instances(), 3);
        EXPECT_EQ(DeStruct::total(), 3);
    }
    EXPECT_EQ(DeStruct::instances(), 1);
    EXPECT_EQ(DeStruct::total(), 3);
    std::destroy_at(reserved);
    EXPECT_EQ(DeStruct::instances(), 0);
    EXPECT_EQ(DeStruct::total(), 3);
    free(reserved);
}

template<class... T> struct PrintType;

TEST(VecMap, traits) {
    using Map = mpgraphs::VecMap<size_t, DeStruct>;
    Map testMap;
    auto it = testMap.begin();
    using It = decltype(it);
    EXPECT_TRUE(std::input_or_output_iterator<It>);
    EXPECT_TRUE(std::weakly_incrementable<It>);
    //BOOST_TEST((requires (It m) { typename std::iter_difference_t<Map>; };));
    EXPECT_TRUE(std::movable<It>);
    EXPECT_TRUE(std::is_object_v<It>);
    EXPECT_TRUE(std::move_constructible<It>);
    EXPECT_TRUE((std::assignable_from<It&, It>));
    EXPECT_TRUE(std::swappable<It>);
    EXPECT_TRUE((std::sentinel_for<It, It>));
    auto begin = std::ranges::begin(testMap);
    auto end = std::ranges::end(testMap);
    EXPECT_EQ(begin, end);
}

TEST(VecMap, insertDelete) {
    DeStruct::total() = 0;
    DeStruct::instances() = 0;
    mpgraphs::VecMap<size_t, DeStruct> testMap;
    constexpr const size_t NUM_INSERT = 100;
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        testMap.insert({i, int(i)});
        EXPECT_TRUE(testMap.contains(i)) << "expected " << i << " in map";
        EXPECT_EQ(DeStruct::instances(), i + 1);
        EXPECT_EQ(testMap[i].value, i);
        EXPECT_EQ(testMap.size(), i + 1);
    }
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    EXPECT_EQ(testMap.size(), NUM_INSERT);
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(testMap.contains(i)) << "expected " << i << " in map";
        EXPECT_EQ(testMap[i].value, i);
    }
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    for (auto kv: testMap) {
        auto k = kv.first;
        auto v = kv.second;
        EXPECT_TRUE(testMap.contains(k));
        EXPECT_EQ(testMap[k].value, v.value);
    }
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    for (const auto kv: static_cast<const decltype(testMap)&>(testMap)) {
        auto k = kv.first;
        auto& v = kv.second;
        EXPECT_TRUE(testMap.contains(k));
        EXPECT_EQ(testMap[k].value, v.value);
    }
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    for (auto it = begin(testMap); it != end(testMap); ++it) { }
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    const auto& constMap = testMap;
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    EXPECT_EQ(std::distance(testMap.begin(), testMap.end()), testMap.size());
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    EXPECT_EQ(std::distance(constMap.begin(), constMap.end()), testMap.size());
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    //PrintType<std::ranges::iterator_t<decltype(testMap)>, std::ranges::range_difference_t<decltype(testMap)>> { };

    //std::ranges::distance(testMap);
    EXPECT_EQ(std::ranges::distance(testMap), testMap.size());
    //for (auto [i, kv]: ranges::enumerate(testMap)) {
    //    auto k = kv.first;
    //    auto v = kv.second;
    //    BOOST_TEST(testMap[k] == v);
    //    BOOST_TEST(testMap.at(k) == v);
    //    BOOST_TEST(k == i);
    //}
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(testMap.contains(i)) << "expected " << i << " in map";
        EXPECT_EQ(DeStruct::instances(), NUM_INSERT - i);
        testMap.erase(i);
        EXPECT_TRUE(!testMap.contains(i)) << "expected " << i << " to be deleted";
        EXPECT_EQ(DeStruct::instances(), NUM_INSERT - i - 1);
        EXPECT_EQ(testMap.size(), NUM_INSERT - i - 1);
    }
    EXPECT_EQ(testMap.size(), 0);
    EXPECT_TRUE(testMap.empty());
    auto it1 = testMap.begin();
    auto it2 = testMap.end();
    EXPECT_EQ(it1, it2);
    EXPECT_EQ(DeStruct::instances(), 0);
}

void test_insert_delete(auto& testMap) {
    constexpr const size_t NUM_INSERT = 100;
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        testMap.insert({i, int(i)});
        EXPECT_TRUE(testMap.contains(i)) << "expected " << i << " in map";
        EXPECT_EQ(testMap[i].value, i);
        EXPECT_EQ(testMap.size(), i + 1);
    }
    EXPECT_EQ(testMap.size(), NUM_INSERT);
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(testMap.contains(i)) << "expected " << i << " in map";
        EXPECT_EQ(testMap[i].value, i);
    }
    for (auto kv: testMap) {
        auto k = kv.first;
        auto v = kv.second;
        EXPECT_TRUE(testMap.contains(k));
        EXPECT_EQ(testMap[k].value, v.value);
    }
    for (const auto& kv: static_cast<const decltype(testMap)&>(testMap)) {
        auto k = kv.first;
        auto& v = kv.second;
        EXPECT_TRUE(testMap.contains(k));
        EXPECT_EQ(testMap[k].value, v.value);
    }
    for (auto it = begin(testMap); it != end(testMap); ++it) { }
    const auto& constMap = testMap;
    EXPECT_EQ(std::distance(testMap.begin(), testMap.end()), testMap.size());
    EXPECT_EQ(std::distance(constMap.begin(), constMap.end()), testMap.size());
    //PrintType<std::ranges::iterator_t<decltype(testMap)>, std::ranges::range_difference_t<decltype(testMap)>> { };

    //std::ranges::distance(testMap);
    //ranges::distance(testMap) == testMap.size();
    //for (auto [i, kv]: ranges::enumerate(testMap)) {
    //    auto k = kv.first;
    //    auto v = kv.second;
    //    BOOST_TEST(testMap[k] == v);
    //    BOOST_TEST(testMap.at(k) == v);
    //    BOOST_TEST(k == i);
    //}
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(testMap.contains(i)) << "expected " << i << " in map";
        testMap.erase(i);
        EXPECT_TRUE(!testMap.contains(i)) << "expected " << i << " to be deleted";
        EXPECT_EQ(testMap.size(), NUM_INSERT - i - 1);
    }
    EXPECT_EQ(testMap.size(), 0);
}

TEST(VecMap, insertDeleteCleared) {
    {
        SCOPED_TRACE("Timestamp=bool");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, bool> testMap;
        test_insert_delete(testMap);
        testMap.clear();
        test_insert_delete(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("Timestamp=uint8_t");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, std::uint8_t> testMap;
        test_insert_delete(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("Timestamp=uint8_t, cleared map");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, std::uint8_t> testMap;
        test_insert_delete(testMap);
        for (size_t i = 0; i < 300; ++i) {
            testMap.emplace(0, 0);
            testMap.clear();
        }
        test_insert_delete(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("Timestamp=char, cleared map");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, char> testMap;
        test_insert_delete(testMap);
        for (size_t i = 0; i < 300; ++i) {
            testMap.emplace(0, 0);
            testMap.clear();
        }
        test_insert_delete(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
}

void test_insert_delete2(auto& testMap) {
    constexpr const size_t NUM_INSERT = 100;
    for (size_t i = 0; i <= NUM_INSERT; ++i) {
        if (i < NUM_INSERT) {
            auto it = testMap.emplace(i, i);
            EXPECT_EQ(testMap.size(), ((i > 0) ? 2 : 1));
            EXPECT_TRUE(it.second);
            EXPECT_EQ(it.first->second.value, i);
        }
        if (i > 0) {
            EXPECT_TRUE(testMap.erase(i - 1));
            EXPECT_TRUE(!testMap.erase(i - 1));
        }
    }
}
TEST(VecMap, insertDelete2) {
    {
        SCOPED_TRACE("Timestamp=bool");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, bool> testMap;
        test_insert_delete2(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("Timestamp=uint8_t");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, std::uint8_t> testMap;
        test_insert_delete2(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("Timestamp=uint8_t, cleared map");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, std::uint8_t> testMap;
        test_insert_delete2(testMap);
        for (size_t i = 0; i < 255; ++i) {
            testMap.emplace(0, 0);
            testMap.clear();
        }
        test_insert_delete2(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("Timestamp=char, cleared map");
        DeStruct::total() = 0;
        DeStruct::instances() = 0;
        mpgraphs::VecMap<size_t, DeStruct, char> testMap;
        test_insert_delete2(testMap);
        for (size_t i = 0; i < 255; ++i) {
            testMap.emplace(0, 0);
            testMap.clear();
        }
        test_insert_delete2(testMap);
    }
    EXPECT_EQ(DeStruct::instances(), 0);
}

TEST(VecMap, copyMove) {
    DeStruct::total() = 0;
    DeStruct::instances() = 0;
    constexpr size_t NUM_INSERT = 100;
    mpgraphs::VecMap<size_t, DeStruct> testMap(NUM_INSERT);
    EXPECT_GE(testMap.capacity() , NUM_INSERT);
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        auto it = testMap.try_emplace(2 * i, 2 * i);
        EXPECT_EQ(testMap.size(), i + 1);
        EXPECT_TRUE(it.second);
        EXPECT_EQ(it.first->second.value, 2 * i);
        EXPECT_TRUE(testMap.contains(2 * i));
    }
    EXPECT_EQ(testMap.size(), NUM_INSERT);
    EXPECT_EQ(DeStruct::instances(), testMap.size());
    auto testMap2 = testMap;
    EXPECT_EQ(DeStruct::instances(), (2 * testMap.size()));
    EXPECT_GE(testMap.capacity() , (NUM_INSERT * 2) - 1);
    EXPECT_EQ(testMap2.size(), testMap.size());
    for (auto kv: testMap) {
        auto k = kv.first;
        auto& v = kv.second;
        EXPECT_TRUE(testMap2.contains(k));
        EXPECT_EQ(testMap2.at(k).value, v.value);
    }
    for (auto kv: testMap2) {
        auto k = kv.first;
        auto& v = kv.second;
        EXPECT_TRUE(testMap.contains(k));
        EXPECT_EQ(testMap.at(k).value, v.value);
    }
    decltype(testMap) testMap5 = testMap2;
    {
        decltype(testMap) testMap3;
        size_t totalCreated = DeStruct::total();
        testMap3 = std::move(testMap2);
        EXPECT_EQ(testMap2.size(), 0);
        EXPECT_EQ(testMap3.size(), testMap.size());
        EXPECT_EQ(DeStruct::instances(), (3 * testMap.size()));
        EXPECT_EQ(totalCreated, DeStruct::total());
        for (auto kv: testMap) {
            auto k = kv.first;
            auto &v = kv.second;
            EXPECT_TRUE(testMap3.contains(k));
            EXPECT_EQ(testMap3.at(k).value, v.value);
        }
        std::swap(testMap2, testMap3);
        EXPECT_EQ(testMap3.size(), 0);
        EXPECT_EQ(testMap2.size(), testMap.size());
        EXPECT_EQ(DeStruct::instances(), (3 * testMap.size()));
        EXPECT_EQ(totalCreated, DeStruct::total());

        auto testMap4(std::move(testMap2));
        EXPECT_EQ(testMap2.size(), 0);
        EXPECT_EQ(testMap4.size(), testMap.size());
        EXPECT_EQ(DeStruct::instances(), (3 * testMap.size()));
        EXPECT_EQ(totalCreated, DeStruct::total());
        for (auto kv: testMap) {
            auto k = kv.first;
            auto &v = kv.second;
            EXPECT_TRUE(testMap4.contains(k));
            EXPECT_EQ(testMap4.at(k).value, v.value);
            EXPECT_EQ(testMap4.at(k).value, k);
        }

        testMap3 = testMap4;
        EXPECT_EQ(DeStruct::instances(), (4 * testMap.size()));
        for (auto kv: testMap) {
            kv.second = 1337;
        }
        for (auto kv: testMap3) {
            auto k = kv.first;
            auto &v = kv.second;
            EXPECT_TRUE(testMap3.contains(k));
            EXPECT_EQ(testMap3.at(k).value, v.value);
            EXPECT_EQ(testMap3.at(k).value, k);
            EXPECT_TRUE(testMap.contains(k));
            EXPECT_EQ(testMap.at(k).value, 1337);
        }
        EXPECT_EQ(DeStruct::instances(), (4 * testMap.size()));
    } // destructs testMap3 and testMap4
    EXPECT_EQ(DeStruct::instances(), (2 * testMap.size()));
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(testMap.contains(2 * i));
        EXPECT_EQ(testMap[2*i].value, 1337);
        EXPECT_TRUE(testMap5.contains(2*i));
        EXPECT_EQ(testMap5[2*i].value, (2*i));
    }
}

TEST(VecMap, findContains) {
    constexpr size_t NUM_INSERT = 100;
    mpgraphs::VecMap<size_t, DeStruct> testMap(NUM_INSERT);
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        testMap.insert_or_assign(2*i, 2*i);
        auto it = testMap.find(2*i);
        auto it2 = testMap.find(2*i + 1);
        EXPECT_NE(it , testMap.end());
        EXPECT_EQ(it2, testMap.end());
        EXPECT_EQ(it->second.value, 2*i);
        EXPECT_EQ(testMap.count(2*i), 1);
        EXPECT_EQ(testMap.count(2*i + 1), 0);
    }
}

template<class Timestamp = bool>
void test_clear() {
    DeStruct::total() = 0;
    DeStruct::instances() = 0;
    constexpr size_t NUM_INSERT = 10;
    constexpr size_t NUM_CLEAR = ssize_t(std::numeric_limits<Timestamp>::max()) - ssize_t(std::numeric_limits<Timestamp>::min());
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> testMap(NUM_INSERT);
    EXPECT_TRUE(testMap.empty());
    EXPECT_EQ(testMap.begin(), testMap.end());
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        testMap[i] = i;
    }
    EXPECT_EQ(DeStruct::instances(), NUM_INSERT);
    EXPECT_EQ(testMap.size(), NUM_INSERT);
    EXPECT_TRUE(!testMap.empty());
    EXPECT_NE(testMap.begin(), testMap.end());
    EXPECT_GE(testMap.capacity() , NUM_INSERT);
    for (size_t i = 1; i < NUM_CLEAR; ++i) {
        testMap.clear();
        EXPECT_LE(DeStruct::instances() , NUM_INSERT);
        EXPECT_EQ(testMap.size(), 0);
        EXPECT_TRUE(testMap.empty());
        EXPECT_EQ(testMap.begin(), testMap.end());
        testMap.emplace(i % NUM_INSERT, 1337);
        EXPECT_LE(DeStruct::instances() , NUM_INSERT);
        EXPECT_GE(testMap.capacity() , NUM_INSERT);
        EXPECT_EQ(testMap.size(), 1);
        EXPECT_TRUE(!testMap.empty());
        EXPECT_NE(testMap.begin() , testMap.end());
    }
    EXPECT_NE(DeStruct::instances() , 0);
    EXPECT_LE(DeStruct::instances() , NUM_INSERT);
    testMap.clear();
    EXPECT_EQ(testMap.size(), 0);
    EXPECT_TRUE(testMap.empty());
    EXPECT_EQ(testMap.begin(), testMap.end());
    EXPECT_EQ(DeStruct::instances(), 0);
}

TEST(VecMap, clear) {
    {
        SCOPED_TRACE("bool timestamp");
        test_clear<bool>();
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("uint8_t timestamp");
        test_clear<uint8_t>();
    }
    EXPECT_EQ(DeStruct::instances(), 0);
    {
        SCOPED_TRACE("char timestamp");
        test_clear<char>();
    }
    EXPECT_EQ(DeStruct::instances(), 0);
}

template<class Timestamp = bool>
void test_nodes(size_t num_insert) {
    DeStruct::total() = 0;
    DeStruct::instances() = 0;
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> testMap(num_insert);
    EXPECT_TRUE(testMap.empty());
    EXPECT_EQ(testMap.begin(), testMap.end());
    std::vector<size_t> indices;
    indices.reserve(num_insert);
    for (size_t i = 0; i < num_insert; ++i) {
        testMap[i] = i;
        indices.push_back(i);
        EXPECT_TRUE(testMap.contains(i));
    }
    EXPECT_EQ(DeStruct::instances(), num_insert);
    EXPECT_EQ(testMap.size(), num_insert);
    EXPECT_TRUE(!testMap.empty());
    EXPECT_NE(testMap.begin() , testMap.end());
    EXPECT_GE(testMap.capacity() , num_insert);
    for (auto i: indices) {
        auto node = testMap.extract(i);
        EXPECT_TRUE(!testMap.contains(i));
        EXPECT_EQ(DeStruct::instances(), num_insert);
        testMap.insert(std::move(node));
        EXPECT_TRUE(testMap.contains(i));
        EXPECT_EQ(testMap[i].value, i);
        EXPECT_EQ(DeStruct::instances(), num_insert);
    }
    EXPECT_EQ(DeStruct::instances(), num_insert);
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> other;
    for (size_t count = 1; auto i: indices) {
        auto node = testMap.extract(i);
        EXPECT_EQ(DeStruct::instances(), num_insert);
        EXPECT_EQ(testMap.size(), indices.size() - count);
        other.insert(std::move(node));
        EXPECT_EQ(other.size(), count);
        EXPECT_TRUE(!testMap.contains(i));
        EXPECT_TRUE(other.contains(i));
        EXPECT_EQ(other[i].value, i);
        EXPECT_EQ(DeStruct::instances(), num_insert);
        EXPECT_EQ(testMap.size() + other.size(), num_insert);
        ++count;
    }
    EXPECT_EQ(DeStruct::instances(), num_insert);
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> thirdMap;
    for (auto i: indices) {
        auto node = testMap.extract(i);
        EXPECT_EQ(DeStruct::instances(), num_insert);
        EXPECT_TRUE(!testMap.contains(i));
        EXPECT_TRUE(node.empty());
        thirdMap.insert(std::move(node));
        EXPECT_TRUE(!thirdMap.contains(i));
        EXPECT_EQ(DeStruct::instances(), num_insert);
    }
    for (auto i: indices) {
        auto node = other.extract(i);
        EXPECT_TRUE(!other.contains(i));
        EXPECT_TRUE(!node.empty());
    }
    EXPECT_EQ(DeStruct::instances(), 0);
}

TEST(VecMap, nodesBool) {
    test_nodes<bool>(10);
}
TEST(VecMap, nodesUint8t) {
    test_nodes<uint8_t>(10);
}
TEST(VecMap, nodesChar) {
    test_nodes<char>(10);
}

template<class Timestamp = bool>
void test_copy(size_t num_insert) {
    DeStruct::total() = 0;
    DeStruct::instances() = 0;
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> testMap(num_insert);
    for (size_t i = 0; i < num_insert; ++i) {
        testMap.insert_or_assign(i, i);
    }
    EXPECT_EQ(testMap.size(), num_insert);
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> otherMap = testMap;
    EXPECT_EQ(testMap.size(), num_insert);
    EXPECT_EQ(otherMap.size(), num_insert);
    for (auto kv: testMap) {
        EXPECT_TRUE(otherMap.contains(kv.first));
        EXPECT_TRUE(otherMap[kv.first].value == kv.second.value);
    }
    EXPECT_TRUE(!std::ranges::empty(testMap));
    mpgraphs::VecMap<size_t, DeStruct, Timestamp> thirdMap;
    testMap = thirdMap;
    EXPECT_TRUE(testMap.empty());
    EXPECT_TRUE(std::ranges::empty(testMap));
}

TEST(VecMap, copyBool) {
        test_copy<bool>(10);
}
TEST(VecMap, copyUint8t) {
        test_copy<uint8_t>(10);
}
TEST(VecMap, copyChar) {
        test_copy<char>(10);
}

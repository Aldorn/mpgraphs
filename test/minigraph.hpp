//
// Created by max on 27.10.22.
//
#ifndef MPGRAPHS_MINIGRAPH_HPP
#define MPGRAPHS_MINIGRAPH_HPP

#include <ankerl/unordered_dense.h>
#include <mpgraphs/common.hpp>

namespace mpgraphs {
template<class VertexData = Empty, EdgeDirection Dir = EdgeDirection::Undirected>
class SimpleGraph {
public:
    using VertexDescriptor = size_t;
    using EdgeDescriptor = std::pair<VertexDescriptor, VertexDescriptor>;
    static constexpr const EdgeDirection Direction = Dir;

    struct DirectedVertexEntry {
        set<VertexDescriptor> m_outNeighbors;
        set<VertexDescriptor> m_inNeighbors;
        VertexData data;

        template<class... Args>
        DirectedVertexEntry(Args... args) : data{args...} {}

        set<VertexDescriptor>& outNeighbors() { return m_outNeighbors; }
        const set<VertexDescriptor>& outNeighbors() const { return m_outNeighbors; }
        set<VertexDescriptor>& inNeighbors() { return m_inNeighbors; }
        const set<VertexDescriptor>& inNeighbors() const { return m_inNeighbors; }
    };

    struct UndirectedVertexEntry {
        set<VertexDescriptor> neighbors;
        VertexData data;

        template<class... Args>
        UndirectedVertexEntry(Args... args) : data{args...} {}

        set<VertexDescriptor>& outNeighbors() { return neighbors; }
        const set<VertexDescriptor>& outNeighbors() const { return neighbors; }
        set<VertexDescriptor>& inNeighbors() { return neighbors; }
        const set<VertexDescriptor>& inNeighbors() const { return neighbors; }
    };
    using VertexEntry = std::conditional_t<Dir == EdgeDirection::Undirected, UndirectedVertexEntry, DirectedVertexEntry>;

    template<class... Args>
    void addVertex(VertexDescriptor id, Args... args) {
        if (!hasVertex(id)) {
            m_vertices[id] = {args...};
        }
    }

    void removeVertex(VertexDescriptor vertex) {
        if (hasVertex(vertex)) {
            for (auto w: neighbors(vertex)) {
                m_vertices.at(w).inNeighbors().erase(vertex);
            }
            for (auto w: inNeighbors(vertex)) {
                m_vertices.at(w).outNeighbors().erase(vertex);
            }
            m_vertices.erase(vertex);
        }
    }

    void removeEdge(VertexDescriptor s, VertexDescriptor t) {
        removeEdge({s, t});
    }

    void removeEdge(EdgeDescriptor edge) {
        auto [s, t] = endpoints(edge);
        if (hasVertex(s) && hasVertex(t)) {
            m_vertices.at(s).outNeighbors().erase(t);
            m_vertices.at(t).inNeighbors().erase(s);
        }
    }

    void addEdge(VertexDescriptor source, VertexDescriptor target) {
        assert(hasVertex(source) && hasVertex(target));
        m_vertices.at(source).outNeighbors().insert(target);
        m_vertices.at(target).inNeighbors().insert(source);
    }

    inline bool hasVertex(VertexDescriptor vertex) const {
        return m_vertices.contains(vertex);
    }

    inline bool hasEdge(VertexDescriptor source, VertexDescriptor target) const {
        return hasVertex(source) && m_vertices.at(source).outNeighbors().contains(target);
    }

    inline bool hasEdge(EdgeDescriptor edge) const {
        return hasEdge(edge.first, edge.second);
    }

    inline VertexDescriptor source(EdgeDescriptor edge) const {
        return edge.first;
    }

    inline VertexDescriptor target(EdgeDescriptor edge) const {
        return edge.second;
    }

    inline std::pair<VertexDescriptor, VertexDescriptor> endpoints(EdgeDescriptor edge) const {
        return edge;
    }

    const auto& neighbors(VertexDescriptor vertex) const {
        assert(hasVertex(vertex));
        return m_vertices.at(vertex).outNeighbors();
    }

    const auto& inNeighbors(VertexDescriptor vertex) const {
        assert(hasVertex(vertex));
        return m_vertices.at(vertex).inNeighbors();
    }

    inline size_t inDegree(VertexDescriptor vertex) const {
        assert(hasVertex(vertex));
        return m_vertices.at(vertex).inNeighbors().size();
    }

    inline size_t outDegree(VertexDescriptor vertex) const {
        assert(hasVertex(vertex));
        return m_vertices.at(vertex).outNeighbors().size();
    }

    inline size_t degree(VertexDescriptor vertex) const {
        assert(hasVertex(vertex));
        if constexpr (Dir == EdgeDirection::Undirected) {
            return m_vertices.at(vertex).outNeighbors().size();
        } else {
            return m_vertices.at(vertex).outNeighbors().size() + m_vertices.at(vertex).inNeighbors().size();
        }
    }

    inline size_t numVertices() const {
        return m_vertices.size();
    }

    inline size_t numEdges() const {
        return ranges::distance(edges());
    }

    inline auto vertices() const {
        return ranges::transform_view(m_vertices, [](auto& kv) {
            return kv.first;
        });
    }

    inline auto edges() const {
        return ranges::transform_view(m_vertices, [this](auto& kv) {
            auto vertex = kv.first;
            return ranges::transform_view(
                    neighbors(vertex),
                    [vertex](auto w) {
                        return std::pair(vertex, w);
                    }
            );
        })
        | ranges::views::join
        | ranges::views::filter([](auto edge) {
            return Dir != EdgeDirection::Undirected || edge.first <= edge.second;
        });
    }

    inline VertexData& getVertex(VertexDescriptor vertex) {
        assert(hasVertex(vertex));
        return m_vertices.at(vertex).data;
    }

    inline const VertexData& getVertex(VertexDescriptor vertex) const {
        assert(hasVertex(vertex));
        return m_vertices.at(vertex).data;
    }

private:
    map<VertexDescriptor, VertexEntry> m_vertices;
};
}

#endif //MPGRAPHS_MINIGRAPH_HPP

//
// Created by max on 10.11.22.
//
#include <gtest/gtest.h>
#include <fmt/format.h>

#include "mpgraphs/vecgraph.hpp"
#include <algorithm>
#include <random>
#include <span>

namespace mpgraphs {
template<class Vertex, EdgeDirection Dir>
void testGraphSimple() {
    VecGraph<Vertex, Dir> graph;
    std::vector<size_t> vertices;
    std::minstd_rand0 rng(4711);
    for (size_t i = 0; i < 10; ++i) {
        vertices.push_back(i);
        graph.getOrAddVertex(i);
        EXPECT_TRUE(graph.hasVertex(i));
    }
    ranges::shuffle(vertices, rng);
    std::uniform_int_distribution dist(size_t(0), vertices.size() - 1);
    for (size_t i = 0; i < 5 * vertices.size(); ++i) {
        auto source = dist(rng);
        auto target = dist(rng);
        graph.addEdge(source, target);
        EXPECT_TRUE(graph.hasEdge(source, target));
    }
    size_t mid = vertices.size() / 2;
    auto kept = std::span(vertices).subspan(0, mid);
    auto deleted = std::span(vertices).subspan(mid);
    for (auto v: deleted) {
        graph.removeVertex(v);
        EXPECT_TRUE(!graph.hasVertex(v));
    }
    for (auto v: kept) {
        for (auto w: graph.neighbors(v)) {
            EXPECT_TRUE(graph.hasVertex(w));
            EXPECT_TRUE(graph.hasEdge(v, w));
        }
        for (auto w: graph.inNeighbors(v)) {
            EXPECT_TRUE(graph.hasVertex(w));
            EXPECT_TRUE(graph.hasEdge(w, v));
        }
    }
    for (auto e: graph.edges()) {
        auto [s, t] = graph.endpoints(e);
        EXPECT_TRUE(graph.hasVertex(s));
        EXPECT_TRUE(graph.hasVertex(t));
        EXPECT_TRUE(graph.hasEdge(s, t));
        EXPECT_TRUE(ranges::contains(graph.neighbors(s), t));
    }
    for (auto v: deleted) {
        graph.getOrAddVertex(v);
        EXPECT_TRUE(graph.hasVertex(v));
        EXPECT_TRUE(graph.inDegree(v) == 0);
        EXPECT_TRUE(graph.outDegree(v) == 0);
    }
    {
        SCOPED_TRACE("after re-insertion");
        for (auto v: kept) {
            for (auto w: graph.neighbors(v)) {
                EXPECT_TRUE(graph.hasVertex(w));
                EXPECT_TRUE(graph.hasEdge(v, w));
            }
            for (auto w: graph.inNeighbors(v)) {
                EXPECT_TRUE(graph.hasVertex(w));
                EXPECT_TRUE(graph.hasEdge(w, v));
            }
        }
        for (auto e: graph.edges()) {
            auto [s, t] = graph.endpoints(e);
            EXPECT_TRUE(graph.hasVertex(s));
            EXPECT_TRUE(graph.hasVertex(t));
            EXPECT_TRUE(graph.hasEdge(s, t));
            EXPECT_TRUE(ranges::contains(graph.neighbors(s), t));
        }
    }
}

TEST(VecGraph, SimpleUndirected) {
    testGraphSimple<int, EdgeDirection::Undirected>();
}
TEST(VecGraph, SimpleBidirectional) {
    testGraphSimple<int, EdgeDirection::Bidirectional>();
}

template<class Vertex, EdgeDirection Dir>
void testGraphCompress(size_t numInsert = 20) {
    VecGraph<Vertex, Dir> graph;
    std::vector<size_t> vertices;
    std::minstd_rand0 rng(4711);
    for (size_t i = 0; i < numInsert; ++i) {
        vertices.push_back(graph.addVertex(Vertex(i)));
        EXPECT_TRUE(graph.hasVertex(i));
    }
    std::uniform_int_distribution dist(size_t(0), vertices.size() - 1);
    for (size_t i = 0; i < 5 * vertices.size(); ++i) {
        auto source = dist(rng);
        auto target = dist(rng);
        graph.addEdge(source, target);
        EXPECT_TRUE(graph.hasEdge(source, target));
    }
    ranges::shuffle(vertices, rng);
    size_t mid = vertices.size() / 2;
    auto kept = std::span(vertices).subspan(0, mid);
    auto deleted = std::span(vertices).subspan(mid);
    for (auto v: deleted) {
        graph.removeVertex(v);
        EXPECT_TRUE(!graph.hasVertex(v));
    }
    std::vector<size_t> newIndex(numInsert + 1, 0);
    for (auto v: kept) {
        newIndex[v + 1] = 1;
    }
    for (size_t i = 1; i < newIndex.size(); ++i) {
        newIndex[i] += newIndex[i - 1];
    }
    VecGraph<Vertex, Dir> graphCopy = graph;
    graph.compress();
    for (size_t i = 0; i < graph.numVertices(); ++i) {
        EXPECT_TRUE(graph.hasVertex(i)) << "expected vertex " << i;
    }
    for (auto v: graphCopy.vertices()) {
        {
            SCOPED_TRACE(fmt::format("vertex {} (mapped {} )", v, newIndex[v]));
            EXPECT_TRUE(graph.hasVertex(newIndex[v]));
            if constexpr (Dir == mpgraphs::EdgeDirection::Undirected) {
                EXPECT_EQ(graph.degree(newIndex[v]), graphCopy.degree(v));
            }
            EXPECT_EQ(graph.outDegree(newIndex[v]), graphCopy.outDegree(v));
            EXPECT_EQ(graph.inDegree(newIndex[v]), graphCopy.inDegree(v));
            EXPECT_EQ(graph.getVertex(newIndex[v]), graphCopy.getVertex(v));
            for (auto w: graphCopy.neighbors(v)) {
                EXPECT_TRUE(graph.hasEdge(newIndex[v], newIndex[w]));
            }
            for (auto wx: ranges::zip_view(graphCopy.neighbors(v), graph.neighbors(newIndex[v]))) {
                auto [w, x] = wx;
                auto w2 = newIndex[w];
                EXPECT_EQ(w2, x);
            }
            if constexpr (Dir == mpgraphs::EdgeDirection::Bidirectional) {
                for (auto wx: ranges::zip_view(graphCopy.inNeighbors(v), graph.inNeighbors(newIndex[v]))) {
                    auto [w, x] = wx;
                    auto w2 = newIndex[w];
                    EXPECT_EQ(w2, x);
                }
            }
        }
    }
}

TEST(VecGraph, CompressUndirected) {
    testGraphCompress<int, EdgeDirection::Undirected>();
}
TEST(VecGraph, CompressBidirectional) {
    testGraphCompress<int, EdgeDirection::Bidirectional>();
}
TEST(VecGraph, CompressDirected) {
    testGraphCompress<int, EdgeDirection::Directed>();
}

}

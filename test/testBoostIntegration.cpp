//
// Created by max on 08.08.22.
//

#include <boost/test/unit_test.hpp>
#include <boost/graph/graph_concepts.hpp>
#include "mpgraphs/boost_adapter.hpp"

using mpgraphs::MapGraph, mpgraphs::VecGraph, mpgraphs::EdgeDirection, mpgraphs::Empty;

template<class... T>
struct PrintType;

void _bidirectional_concepts() {
    using Graph = MapGraph<mpgraphs::Empty, mpgraphs::Empty, mpgraphs::EdgeDirection::Bidirectional>;
    boost::GraphConcept<Graph> _g = {};
    boost::IncidenceGraphConcept<Graph> _ig = {};
    boost::BidirectionalGraphConcept<Graph> _bg = {};
    boost::AdjacencyGraphConcept<Graph> _ag = {};
    boost::VertexListGraphConcept<Graph> _vlg = {};
    boost::EdgeListGraphConcept<Graph> _elg = {};
    boost::AdjacencyMatrixConcept<Graph> _am = {};
    boost::MutableGraphConcept<Graph> _mg = {};
}

void _undirected_concepts() {
    using Graph = MapGraph<mpgraphs::Empty, mpgraphs::Empty, mpgraphs::EdgeDirection::Undirected>;
    boost::GraphConcept<Graph> _g = {};
    boost::IncidenceGraphConcept<Graph> _ig = {};
    boost::BidirectionalGraphConcept<Graph> _bg = {};
    boost::AdjacencyGraphConcept<Graph> _ag = {};
    boost::VertexListGraphConcept<Graph> _vlg = {};
    boost::EdgeListGraphConcept<Graph> _elg = {};
    boost::AdjacencyMatrixConcept<Graph> _am = {};
    boost::MutableGraphConcept<Graph> _mg = {};
}

void _directed_concepts() {
    using Graph = MapGraph<mpgraphs::Empty, mpgraphs::Empty, mpgraphs::EdgeDirection::Directed>;
    boost::GraphConcept<Graph> _g = {};
    boost::IncidenceGraphConcept<Graph> _ig = {};
    boost::AdjacencyGraphConcept<Graph> _ag = {};
    boost::VertexListGraphConcept<Graph> _vlg = {};
    boost::EdgeListGraphConcept<Graph> _elg = {};
    boost::AdjacencyMatrixConcept<Graph> _am = {};
    boost::MutableGraphConcept<Graph> _mg = {};
}

BOOST_AUTO_TEST_CASE(testStandardFunctionsMapGraph) {
    using Graph = MapGraph<Empty, Empty, EdgeDirection::Undirected>;
    Graph graph;
    auto a = add_vertex(graph);
    auto b = add_vertex(graph);
    auto c = add_vertex(graph);
    auto d = add_vertex(graph);
    auto e = add_vertex(graph);

    add_edge(a, b, graph);
    add_edge(a, c, graph);
    add_edge(a, d, graph);
    add_edge(d, e, graph);
    add_edge(b, e, graph);
    add_edge(c, b, graph);

    mpgraphs::set<Graph::VertexDescriptor> all_vertices;
    auto vertex_range = vertices(graph);
    for (auto it = vertex_range.first; it != vertex_range.second; ++it) {
        all_vertices.insert(*it);
    }
    BOOST_TEST(all_vertices.size() == 5);
    BOOST_TEST(all_vertices.contains(a));
    BOOST_TEST(all_vertices.contains(b));
    BOOST_TEST(all_vertices.contains(c));
    BOOST_TEST(all_vertices.contains(d));
    BOOST_TEST(all_vertices.contains(e));

    BOOST_TEST(graph.edge(a, b).has_value());
    BOOST_TEST(graph.edge(a, c).has_value());
    BOOST_TEST(graph.edge(a, d).has_value());
    BOOST_TEST(graph.edge(d, e).has_value());
    BOOST_TEST(graph.edge(b, c).has_value());

    BOOST_TEST(graph.numVertices() == 5);
    BOOST_TEST(num_vertices(graph) == 5);
    BOOST_TEST(graph.numEdges() == 6);
    BOOST_TEST(num_edges(graph) == 6);

    for (auto v: graph.vertices()) {
        BOOST_TEST(degree(v, graph) == graph.degree(v));
        BOOST_TEST(in_degree(v, graph) == graph.inDegree(v));
    }

    for (auto e: graph.edges()) {
        BOOST_TEST(source(e, graph) == graph.source(e));
        BOOST_TEST(target(e, graph) == graph.target(e));
    }
}

BOOST_AUTO_TEST_CASE(testStandardFunctionsVecGraph, *boost::unit_test::disabled()) {
    using Graph = VecGraph<Empty, EdgeDirection::Undirected>;
    Graph graph;
    auto a = add_vertex(graph);
    auto b = add_vertex(graph);
    auto c = add_vertex(graph);
    auto d = add_vertex(graph);
    auto e = add_vertex(graph);

    add_edge(a, b, graph);
    add_edge(a, c, graph);
    add_edge(a, d, graph);
    add_edge(d, e, graph);
    add_edge(b, e, graph);
    add_edge(c, b, graph);

    mpgraphs::set<Graph::VertexDescriptor> all_vertices;
    auto vertex_range = vertices(graph);
    for (auto it = vertex_range.first; it != vertex_range.second; ++it) {
        all_vertices.insert(*it);
    }
    BOOST_TEST(all_vertices.size() == 5);
    BOOST_TEST(all_vertices.contains(a));
    BOOST_TEST(all_vertices.contains(b));
    BOOST_TEST(all_vertices.contains(c));
    BOOST_TEST(all_vertices.contains(d));
    BOOST_TEST(all_vertices.contains(e));

    BOOST_TEST(graph.edge(a, b).has_value());
    BOOST_TEST(graph.edge(a, c).has_value());
    BOOST_TEST(graph.edge(a, d).has_value());
    BOOST_TEST(graph.edge(d, e).has_value());
    BOOST_TEST(graph.edge(b, c).has_value());

    BOOST_TEST(graph.numVertices() == 5);
    BOOST_TEST(num_vertices(graph) == 5);
    BOOST_TEST(graph.numEdges() == 6);
    BOOST_TEST(num_edges(graph) == 6);

    for (auto v: graph.vertices()) {
        BOOST_TEST(degree(v, graph) == graph.degree(v));
        BOOST_TEST(in_degree(v, graph) == graph.inDegree(v));
    }

    for (auto e: graph.edges()) {
        BOOST_TEST(source(e, graph) == graph.source(e));
        BOOST_TEST(target(e, graph) == graph.target(e));
    }
}

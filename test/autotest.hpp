#ifndef MPGRAPHS_AUTOTEST_HPP
#define MPGRAPHS_AUTOTEST_HPP

#include <set>
#include <gtest/gtest.h>
#include <fmt/format.h>

#include "minigraph.hpp"

using mpgraphs::SimpleGraph;
using mpgraphs::EdgeDirection;

template<std::uniform_random_bit_generator Rng>
size_t randInt(Rng& rng, size_t max) {
    return std::uniform_int_distribution(size_t{0}, (max > 0 ? (max - 1) : size_t{0}))(rng);
}

template<class Graph>
concept HasOutDegree = requires(Graph g, typename Graph::VertexDescriptor v) {
    { g.outDegree(v) } -> std::integral;
};

template<class Graph>
concept HasInDegree = requires(Graph g, typename Graph::VertexDescriptor v) {
    { g.inDegree(v) } -> std::integral;
};

template<class Graph>
concept HasDegree = requires(Graph g, typename Graph::VertexDescriptor v) {
    { g.degree(v) } -> std::integral;
};

template<class Graph>
std::string graphToString(const Graph& graph) {
    auto vertexStrings = graph.vertices()
        | ranges::views::transform([](auto v) { return std::to_string(v); })
        | ranges::to<std::vector>;
    auto vertices = vertexStrings
        | ranges::views::join(std::vector{',', ' '})
        | ranges::to<std::string>;
    std::string to = Graph::Direction == mpgraphs::EdgeDirection::Undirected ? "--" : "->";
    auto edgeStrings = graph.edges()
        | ranges::views::transform([&graph](auto e) { return graph.endpoints(e); })
        | ranges::views::transform([&to](auto st) { return std::to_string(st.first) + to + std::to_string(st.second); })
        | ranges::to<std::vector>;
    auto edges = edgeStrings
        | ranges::views::join(std::vector{',', ' '})
        | ranges::to<std::string>;
    return std::string("graph(") + std::to_string(graph.numVertices()) + " vertices={" + vertices + "}, " + std::to_string(graph.numEdges()) + " edges={" + edges + "}";
}

template<class Graph>
void validateGraphFull(Graph& graph, SimpleGraph<size_t, Graph::Direction>& reference) {
    {
        SCOPED_TRACE(fmt::format("graph: {} \nreference: {}", graphToString(graph), graphToString(reference)));
        EXPECT_EQ(graph.numEdges(), reference.numEdges());
        EXPECT_EQ(graph.numEdges(), ranges::distance(graph.edges()));
        EXPECT_EQ(graph.numVertices(), reference.numVertices());
        EXPECT_EQ(graph.numVertices(), ranges::distance(graph.vertices()));
        for (auto vertex: graph.vertices()) {
            EXPECT_TRUE(reference.hasVertex(graph.getVertex(vertex)));
            if constexpr (HasDegree<decltype(graph)>) {
                EXPECT_EQ(reference.degree(graph.getVertex(vertex)), graph.degree(vertex));
            }
            if constexpr (HasInDegree<decltype(graph)>) {
                EXPECT_EQ(reference.inDegree(graph.getVertex(vertex)), graph.inDegree(vertex));
            }
            if constexpr (HasOutDegree<decltype(graph)>) {
                EXPECT_EQ(reference.outDegree(graph.getVertex(vertex)), graph.outDegree(vertex));
            }
        }
        for (auto vertex: reference.vertices()) {
            EXPECT_TRUE(graph.hasVertex(reference.getVertex(vertex)));
        }
        for (auto edge: graph.edges()) {
            auto [s, t] = graph.endpoints(edge);
            {
                SCOPED_TRACE(fmt::format("vertices s={} and t={}", s, t));
                EXPECT_TRUE(graph.hasEdge(s, t));
                EXPECT_TRUE(graph.hasVertex(s));
                EXPECT_TRUE(graph.hasVertex(t));
                EXPECT_TRUE(reference.hasEdge(graph.getVertex(s), graph.getVertex(t)));
            }
        }
        for (auto edge: reference.edges()) {
            auto [s, t] = reference.endpoints(edge);
            {
                SCOPED_TRACE(fmt::format("vertices s={}, t={}", s, t));
                EXPECT_TRUE(reference.hasVertex(s));
                EXPECT_TRUE(reference.hasVertex(t));
                EXPECT_TRUE(graph.hasEdge(reference.getVertex(s), reference.getVertex(t)));
            }
        }
        for (auto v: graph.vertices()) {
            EXPECT_EQ(graph.outDegree(v), reference.outDegree(v));
            EXPECT_EQ(graph.outDegree(v), ranges::distance(graph.neighbors(v)));
            EXPECT_EQ(graph.outDegree(v), ranges::distance(graph.outEdges(v)));
            for (auto w: graph.neighbors(v)) {
                EXPECT_TRUE(graph.hasEdge(v, w));
            }
            for (auto edge: graph.outEdges(v)) {
                EXPECT_TRUE(graph.hasEdge(v, graph.target(edge)));
            }
            if constexpr (Graph::Direction != mpgraphs::EdgeDirection::Directed) {
                for (auto w: graph.inNeighbors(v)) {
                    EXPECT_TRUE(graph.hasEdge(w, v));
                }
                for (auto edge: graph.inEdges(v)) {
                    EXPECT_TRUE(graph.hasEdge(graph.source(edge), v));
                }
            }
        }
    }
}

template<class Graph, std::uniform_random_bit_generator Rng>
void testAdding(Graph& graph, SimpleGraph<size_t, Graph::Direction>& reference, size_t numVertices, size_t numEdges, Rng& rng) {
    for (size_t i = 0; i < numVertices; ++i) {
        graph.addVertex(i);
        reference.addVertex(i, i);
    }
    EXPECT_EQ(graph.numVertices(), numVertices);
    EXPECT_EQ(graph.numEdges(), 0);

    for (size_t i = 0; i < numEdges; ++i) {
        auto s = randInt(rng, numVertices);
        auto t = randInt(rng, numVertices);

        reference.addEdge(s, t);
        graph.addEdge(s, t);
        {
            SCOPED_TRACE(fmt::format("add edge {} to {}", s, t));
            EXPECT_TRUE(graph.hasEdge(s, t));
            EXPECT_EQ(graph.numEdges(), reference.numEdges());
            //BOOST_TEST(graph.numEdges() == ranges::accumulate(
            //        graph.edges()
            //        | ranges::views::transform([&graph](auto e) { return 1 + (graph.source(e) == graph.target(e));}),
            //        0
            //));
        }
    }
    {
        SCOPED_TRACE(fmt::format("added {} vertices and {} edges", numVertices, numEdges));
        validateGraphFull(graph, reference);
    }
}


template<class Graph, std::uniform_random_bit_generator Rng>
void testRemoving(Graph& graph, SimpleGraph<size_t, Graph::Direction>& reference, Rng& rng) {
    auto vertices = graph.vertices() | ranges::to<std::vector>;
    ranges::shuffle(vertices, rng);
    const size_t numDel = vertices.size() / 2;
    for (size_t i = numDel; i < vertices.size(); ++i) {
        EXPECT_TRUE(graph.hasVertex(vertices[i]));
        reference.removeVertex(graph.getVertex(vertices[i]));
        graph.removeVertex(vertices[i]);
        EXPECT_TRUE(!graph.hasVertex(vertices[i]));
    }
    {
        SCOPED_TRACE(fmt::format("removed {} vertices", (vertices.size() - numDel)));
        validateGraphFull(graph, reference);
    }
    vertices.resize(numDel);
    size_t numDelEdges = 2 * graph.numEdges();
    for (size_t i = 0; i < numDelEdges; ++i) {
        auto st = vertices | ranges::views::sample(2, rng) | ranges::to<std::vector>;
        auto s = st[0];
        auto t = st[1];
        auto edge_pairs = graph.edges()
                | ranges::views::transform([&graph](auto e) {
                    auto [s, t] = graph.endpoints(e);
                    return std::to_string(s) + "-" + std::to_string(t) + ", ";
                })
                | ranges::to<std::vector>;
        auto edges = edge_pairs
                | ranges::views::join
                | ranges::to<std::string>;
        EXPECT_EQ(graph.hasEdge(s, t), reference.hasEdge(graph.getVertex(s), graph.getVertex(t)));
        graph.removeEdge(s, t);
        EXPECT_TRUE(!graph.hasEdge(s, t));
        reference.removeEdge(graph.getVertex(s), graph.getVertex(t));
        EXPECT_TRUE(!reference.hasEdge(graph.getVertex(s), graph.getVertex(t)));
        EXPECT_EQ(graph.hasEdge(s, t), reference.hasEdge(graph.getVertex(s), graph.getVertex(t)));
        EXPECT_EQ(graph.numEdges(), reference.numEdges());
        EXPECT_EQ(graph.numVertices(), reference.numVertices());
        {
            SCOPED_TRACE(fmt::format("removed {} to {} from {}", s, t, edges));
            validateGraphFull(graph, reference);
        }
    }
    {
        SCOPED_TRACE(fmt::format("removed {} random vertex pairs", numDelEdges));
        validateGraphFull(graph, reference);
    }
    for (size_t i = 0; i < numDel; ++i) {
        EXPECT_TRUE(graph.hasVertex(vertices[i]));
        reference.removeVertex(graph.getVertex(vertices[i]));
        graph.removeVertex(vertices[i]);
        EXPECT_TRUE(!graph.hasVertex(vertices[i]));
    }
    {
        SCOPED_TRACE(fmt::format("removed all remaining vertices"));
        validateGraphFull(graph, reference);
    }
}

template<class Graph, std::uniform_random_bit_generator Rng>
void testInteract(Graph& graph, SimpleGraph<size_t, Graph::Direction>& reference, Rng& rng) {
    std::make_tuple(graph, reference, rng);
    //TODO
}

#endif //MPGRAPHS_AUTOTEST_HPP

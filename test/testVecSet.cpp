//
// Created by max on 09.11.22.
//
#include <gtest/gtest.h>
#include "mpgraphs/vecset.hpp"
#include <range/v3/all.hpp>

TEST(VecSet, test_insert_remove_continous) {
    mpgraphs::VecSet<size_t> testSet;
    constexpr size_t NUM_INSERT = 100;
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(!testSet.contains(i));
        EXPECT_EQ(testSet.count(i), 0);
        testSet.insert(i);
        EXPECT_TRUE(testSet.contains(i));
        EXPECT_EQ(testSet.count(i), 1);
    }
    EXPECT_EQ(testSet.size(), NUM_INSERT);
    EXPECT_TRUE(!testSet.empty());
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(testSet.contains(i));
        EXPECT_EQ(testSet.count(i), 1);
        testSet.erase(i);
        EXPECT_TRUE(!testSet.contains(i));
        EXPECT_EQ(testSet.count(i), 0);
    }
    EXPECT_EQ(testSet.size(), 0);
    EXPECT_TRUE(testSet.empty());
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        EXPECT_TRUE(!testSet.contains(i));
        testSet.erase(i);
        EXPECT_EQ(testSet.size(), 0);
    }
}

void testIteration(auto& set) {
    EXPECT_TRUE(set.empty());
    EXPECT_EQ(set.begin(), set.end());
    constexpr size_t NUM_INSERT = 200;
    for (size_t i = 0; i < NUM_INSERT; ++i) {
        set.insert(2 * i);
        EXPECT_TRUE(set.contains(2*i));
        EXPECT_EQ(set.size(), i + 1);
    }
    EXPECT_EQ(set.size(), NUM_INSERT);
    auto it = set.begin();
    EXPECT_EQ(*it, 0);
    size_t previous = 0;
    for (++it; it != set.end(); ++it) {
        EXPECT_EQ(*it, previous + 2);
        previous = *it;
    }
    EXPECT_EQ(previous, NUM_INSERT * 2 - 2);
    bool first = true;
    previous = -1;
    for (auto x: set) {
        if (!first) {
            EXPECT_EQ(x, previous + 2);
        }
        previous = x;
    }
    EXPECT_EQ(previous, NUM_INSERT * 2 - 2);
}

TEST(VecSet, iteration) {
    {
        SCOPED_TRACE("timestamp=bool");
        mpgraphs::VecSet<size_t, bool> testMap;
        testIteration(testMap);
        testMap.clear();
        testIteration(testMap);
        testMap.clear();
        testIteration(testMap);
    }
    {
        SCOPED_TRACE("timestamp=uint8_t");
        mpgraphs::VecSet<size_t, std::uint8_t> testMap;
        {
            SCOPED_TRACE("fresh set");
            testIteration(testMap);
        }
        testMap.clear();
        {
            SCOPED_TRACE("cleared");
            testIteration(testMap);
        }
        testMap.clear();
        for (size_t i = 0; i < 254; ++i) {
            testMap.insert(i % 113);
            testMap.clear();
        }
        {
            SCOPED_TRACE("cleared until reset");
            testIteration(testMap);
        }
    }
    {
        SCOPED_TRACE("timestamp=char");
        mpgraphs::VecSet<size_t, signed char> testMap;
        {
            SCOPED_TRACE("fresh set");
            testIteration(testMap);
        }
        testMap.clear();
        {
            SCOPED_TRACE("cleared");
            testIteration(testMap);
        }
        testMap.clear();
        for (size_t i = 0; i < 254; ++i) {
            testMap.insert(i % 111);
            testMap.clear();
        }
        {
            SCOPED_TRACE("cleared until reset");
            testIteration(testMap);
        }
    }
}
